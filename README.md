# MiWakeUpLightSwarm

A wakeuplight for a Mi Light Dual White lamp with Wildfly Swarm and Angularjs.   

This project is an exact copy of [https://gitlab.com/raginggoblin/MiWakeUpLight](https://gitlab.com/raginggoblin/MiWakeUpLight) but instead of using Spring Boot it is based on WildFly Swarm. 

<p align="center"><img src="https://gitlab.com/raginggoblin/MiWakeUpLightSwarm/raw/master/images/MiWakeUpLightSwarm.png" /></p>

If you are only interested in getting a wakeup light you should checkout [https://gitlab.com/raginggoblin/MiWakeUpLight](https://gitlab.com/raginggoblin/MiWakeUpLight). I just created this project to see if I could create a full-stack application using WildFly Swarm and to compare the two frameworks.

