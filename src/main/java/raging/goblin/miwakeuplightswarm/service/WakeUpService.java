/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplightswarm.service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import lombok.AllArgsConstructor;
import lombok.Data;
import raging.goblin.miwakeuplightswarm.WifiControllerAPI;
import raging.goblin.miwakeuplightswarm.domain.WakeUpSchedule;
import raging.goblin.miwakeuplightswarm.storage.WakeUpScheduleRepository;

@Singleton
public class WakeUpService {
   
   Logger log = Logger.getLogger(WakeUpService.class);

   private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

   private WifiControllerAPI wifiController;
   
   @Inject
   private WakeUpScheduleRepository wakeUpScheduleRepository;

   private Map<Long, ScheduledSchedule> scheduledWakeUps = new HashMap<>();

   @PostConstruct
   public void scheduleAllWakeUps() {
      wifiController = createWifiControllerAPI();
      List<WakeUpSchedule> allSchedules = wakeUpScheduleRepository.findAll();
      allSchedules.forEach(s -> scheduleSingleWakeUp(s));
   }

   public void scheduleDeleted(Long id) {
      ScheduledSchedule scheduledWakeup = scheduledWakeUps.get(id);
      if (scheduledWakeup != null) {
         synchronized (scheduler) {
            scheduledWakeup.getFuture().cancel(true);
         }

         log.infov("Schedule with id {0} canceled", id);

         if (isWakeupAlreadyRunning(scheduledWakeup)) {
            new Thread(() -> switchLightOff()).start();
            log.info("Schedule was already running, so we switched the light off");
         }
      }
   }

   public void scheduleSaved(WakeUpSchedule savedWakeUpSchedule) {
      scheduleDeleted(savedWakeUpSchedule.getId());
      scheduleSingleWakeUp(savedWakeUpSchedule);
   }
   
   private WifiControllerAPI createWifiControllerAPI() {
      try {
         String wificontrollerHost = System.getProperty("raging.goblin.miwakeuplight.wificontroller.host");
         int wificontrollerPort = Integer.valueOf(System.getProperty("raging.goblin.miwakeuplight.wificontroller.port"));
         return new WifiControllerAPI(InetAddress.getByName(wificontrollerHost), wificontrollerPort);
      } catch (UnknownHostException e) {
         System.err.println("Unable to find wificontroller host, it is of no use to go any further");
         e.printStackTrace();
         System.exit(100);
      }
      return null;
   }

   private void switchLightOff() {
      try {
         log.debug("Set brightness to 0%");
         int wificontrollerLight = Integer.valueOf(System.getProperty("raging.goblin.miwakeuplight.wificontroller.light"));
         wifiController.brightness(0, wificontrollerLight);
         Thread.sleep(10000);
         wifiController.off(wificontrollerLight);
         log.debug("Switched light off");
      } catch (InterruptedException | IllegalArgumentException | IOException e) {
         log.error("Unable to switch off light properly", e);
      }
   }

   private boolean isWakeupAlreadyRunning(ScheduledSchedule scheduledWakeup) {
      return Math.abs(scheduledWakeup.getScheduledDateTime().getTime() - new Date().getTime()) < scheduledWakeup
            .getSchedule().getDuration() + scheduledWakeup.getSchedule().getDayLength();
   }

   private void scheduleSingleWakeUp(WakeUpSchedule schedule) {
      if (schedule.isActive() && !schedule.getDaysActive().isEmpty()) {
         int timeBetweenRunners = schedule.getDuration() / 10;
         BrightnessRunner runner = new BrightnessRunner(0, timeBetweenRunners, schedule);
         Date startTime = calculateNextRun(schedule);
         ScheduledFuture<?> runnerFuture = scheduler.schedule(runner, startTime.getTime() - new Date().getTime(), TimeUnit.MILLISECONDS);
         scheduledWakeUps.put(schedule.getId(), new ScheduledSchedule(schedule, runnerFuture, startTime));
         log.infov("Wakeup scheduled: {0} at {1}", schedule, new Date(startTime.getTime() + schedule.getDuration()));
      }
   }

   private Date calculateNextRun(WakeUpSchedule schedule) {
      ZonedDateTime scheduledTime = ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS);
      scheduledTime = scheduledTime.plus(schedule.getTime() - schedule.getDuration(), ChronoUnit.MILLIS);
      if (scheduledTime.isBefore(ZonedDateTime.now())) {
         scheduledTime = scheduledTime.plus(1, ChronoUnit.DAYS);
      }

      while (!schedule.getDaysActive().contains(scheduledTime.getDayOfWeek())) {
         scheduledTime = scheduledTime.plus(1, ChronoUnit.DAYS);
      }

      return Date.from(scheduledTime.toInstant());
   }

   @AllArgsConstructor
   private class BrightnessRunner implements Runnable {

      private int brightness;
      private int timeBetweenRunners;
      private WakeUpSchedule schedule;

      @Override
      public void run() {
         try {
            log.debugv("Set brightness to {0}%", brightness);
            int wificontrollerLight = Integer.valueOf(System.getProperty("raging.goblin.miwakeuplight.wificontroller.light"));
            wifiController.brightness(brightness, wificontrollerLight);
         } catch (IllegalArgumentException | IOException e) {
            log.error("Unable to set brightness to " + brightness, e);
         }

         if (brightness < 100) {
            scheduleNextBrightness();
         } else if (brightness == 100) {
            scheduleDayLightEnds();
         }
      }

      private void scheduleNextBrightness() {
         BrightnessRunner newRunner = new BrightnessRunner(brightness + 10, timeBetweenRunners, schedule);
         Date startTime = new Date(new Date().getTime() + timeBetweenRunners);

         synchronized (scheduler) {
            ScheduledFuture<?> runnerFuture = scheduler.schedule(newRunner, startTime.getTime() - new Date().getTime(), TimeUnit.MILLISECONDS);
            scheduledWakeUps.put(schedule.getId(), new ScheduledSchedule(schedule, runnerFuture, startTime));
         }

         log.debugv("Scheduled next brightness step for {0}", startTime);
      }

      private void scheduleDayLightEnds() {
         Date dayEndTime = new Date(new Date().getTime() + schedule.getDayLength());
         Runnable dayLightEndsRunner = new DayLightEndsRunner(schedule);

         synchronized (scheduler) {
            ScheduledFuture<?> runnerFuture = scheduler.schedule(dayLightEndsRunner, dayEndTime.getTime() - new Date().getTime(), TimeUnit.MILLISECONDS);
            scheduledWakeUps.put(schedule.getId(), new ScheduledSchedule(schedule, runnerFuture, dayEndTime));
         }

         log.debugv("Scheduled daylight end for {0}", dayEndTime);
      }
   }

   @AllArgsConstructor
   private class DayLightEndsRunner implements Runnable {

      private WakeUpSchedule schedule;

      @Override
      public void run() {
         switchLightOff();
         scheduleSingleWakeUp(schedule);
      }
   }

   @Data
   @AllArgsConstructor
   private class ScheduledSchedule {

      private WakeUpSchedule schedule;
      private ScheduledFuture<?> future;
      private Date scheduledDateTime;
   }
}
