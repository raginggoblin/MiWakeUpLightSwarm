/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplightswarm.web;

import java.io.IOException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.jboss.logging.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import raging.goblin.miwakeuplightswarm.domain.WakeUpSchedule;
import raging.goblin.miwakeuplightswarm.service.WakeUpService;
import raging.goblin.miwakeuplightswarm.storage.WakeUpScheduleRepository;

@Stateless
@Path("/wakeupschedules")
public class WakeUpScheduleController {

   Logger log = Logger.getLogger(WakeUpScheduleController.class);

   @Inject
   private WakeUpScheduleRepository wakeUpScheduleRepository;
   @Inject
   private WakeUpService wakeUpService;

   @GET
   @Produces("application/json")
   public List<WakeUpSchedule> getAll() {
      log.info("Requesting all wakeup schedules");
      return wakeUpScheduleRepository.findAll();
   }

   @GET
   @Path("{id}")
   @Produces("application/json")
   public WakeUpSchedule getWakeUpSchedule(@PathParam("id") Long id) {
      log.infov("Requesting wakeupschedule with id {0}", id);
      return wakeUpScheduleRepository.findOne(id);
   }

   @DELETE
   @Path("{id}")
   public void deleteWakeUpSchedule(@PathParam("id") Long id) {
      wakeUpScheduleRepository.delete(id);
      log.infov("WakeUpSchedule with id {0} deleted", id);
      wakeUpService.scheduleDeleted(id);
   }

   @POST
   @Produces("application/json")
   @Consumes("application/json")
   public WakeUpSchedule save(@QueryParam("wakeUpSchedule") String wakeUpSchedule)
         throws JsonParseException, JsonMappingException, IOException {

      WakeUpSchedule wakeUpScheduleObject = new ObjectMapper().readValue(wakeUpSchedule, WakeUpSchedule.class);
      WakeUpSchedule savedWakeUpSchedule = wakeUpScheduleRepository.save(wakeUpScheduleObject);
      log.infov("WakeUpSchedule saved: {0}", savedWakeUpSchedule);

      wakeUpService.scheduleSaved(savedWakeUpSchedule);

      return savedWakeUpSchedule;
   }
}