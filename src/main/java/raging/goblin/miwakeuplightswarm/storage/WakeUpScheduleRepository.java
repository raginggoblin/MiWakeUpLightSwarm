/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplightswarm.storage;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import raging.goblin.miwakeuplightswarm.domain.WakeUpSchedule;

@Stateless
public class WakeUpScheduleRepository {
   
   @PersistenceContext(name="MiWakeUpLighSwarmPersistenceUnit")
   private EntityManager em;
   
   public List<WakeUpSchedule> findAll() {
      CriteriaQuery<WakeUpSchedule> query = em.getCriteriaBuilder().createQuery(WakeUpSchedule.class);
      query.select(query.from(WakeUpSchedule.class));
      return em.createQuery(query).getResultList();
   }

   public WakeUpSchedule findOne(Long id) {
      return em.find(WakeUpSchedule.class, id);
   }
   
   public void delete(Long id) {
      WakeUpSchedule wakeUpSchedule = findOne(id);
      em.remove(wakeUpSchedule);
   }
   
   public WakeUpSchedule save(WakeUpSchedule wakeUpSchedule) {
      if(wakeUpSchedule.getId() != null && findOne(wakeUpSchedule.getId()) != null) {
         em.merge(wakeUpSchedule);
      } else {
         em.persist(wakeUpSchedule);
      }
      return wakeUpSchedule;
   }
}
