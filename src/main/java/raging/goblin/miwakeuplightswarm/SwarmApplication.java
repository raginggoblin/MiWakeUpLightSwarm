/*
 * Copyright 2017, Raging Goblin <https://raginggoblin.wordpress.com/>
 * 
 * This file is part of MiWakeUpLight.
 *
 *  MiWakeUpLight is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MiWakeUpLight is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MiWakeUpLight.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.miwakeuplightswarm;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.config.logging.Level;
import org.wildfly.swarm.datasources.DatasourcesFraction;
import org.wildfly.swarm.jaxrs.JAXRSArchive;
import org.wildfly.swarm.logging.LoggingFraction;

@ApplicationPath("/api")
public class SwarmApplication extends Application {
   
   public static void main(String...args) throws Exception {
      Swarm swarm = new Swarm();
      
      String logFile = System.getProperty("user.dir") + File.separator + "miwakeuplightswarm.log";
      swarm.fraction(
            new LoggingFraction()
              .fileHandler("FILE", f -> {
                Map<String, String> fileProps = new HashMap<>();
                fileProps.put("path", logFile);
                f.file(fileProps);
                f.level(Level.INFO);
                f.formatter("%d{yyyy-MM-dd HH:mm:ss.SSS} %-5p [%c] (%t) %s%e%n");
              })
              .consoleHandler("CONSOLE", c -> {
                 c.level(Level.INFO);
                 c.formatter("%d{yyyy-MM-dd HH:mm:ss.SSS} %-5p [%c] (%t) %s%e%n");
              })
              .rootLogger(Level.INFO, "FILE")
              .rootLogger(Level.INFO, "CONSOLE")
          );
      
      swarm.fraction(new DatasourcesFraction()
            .jdbcDriver("h2", (d) -> {
                d.driverClassName("org.h2.Driver");
                d.xaDatasourceClass("org.h2.jdbcx.JdbcDataSource");
                d.driverModuleName("com.h2database.h2");
            })
            .dataSource("H2DS", (ds) -> {
                ds.driverName("h2");
                ds.connectionUrl("jdbc:h2:file:./miwakeuplightswarm-database;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
                ds.userName("sa");
                ds.password("sa");
            })
         );
      
      swarm.start();
      JAXRSArchive deployment = ShrinkWrap.create(JAXRSArchive.class);
      deployment.addAsLibrary(swarm.createDefaultDeployment());
      deployment.addAllDependencies();
      deployment.staticContent();
      deployment.addPackages(true, Package.getPackage("raging.goblin.miwakeuplightswarm"));
      swarm.deploy(deployment);
  }
}